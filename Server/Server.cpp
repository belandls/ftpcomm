#include "Server.h"

const char * Server::DEFAULT_PORT = "55555";

Server::Server()
{
	_listen_socket = INVALID_SOCKET;
}


Server::~Server()
{
}

bool Server::init(){

	WSAData wsaData;
	int result;
	addrinfo * addResult = NULL, hints;

	cout << "Initialising WSA...";
	result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != 0){
		cerr << "WSAStartup failed : " << result << endl;
		return false;
	}
	else{
		cout << "OK" << endl;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	result = getaddrinfo(NULL, DEFAULT_PORT, &hints, &addResult);
	if (result != 0){
		cerr << "getaddrinfo failed : " << result << endl;
		WSACleanup();
		return false;
	}

	cout << "Creating listen socket...";
	_listen_socket = socket(addResult->ai_family, addResult->ai_socktype, addResult->ai_protocol);
	if (_listen_socket == INVALID_SOCKET){
		cerr << "Socket creation Failed : " << WSAGetLastError() << endl;
		freeaddrinfo(addResult);
		WSACleanup();
		return false;
	}else{
		cout << "OK." << endl;
	}

	cout << "Binding socket...";
	result = bind(_listen_socket, addResult->ai_addr, (int)addResult->ai_addrlen);
	if (result == SOCKET_ERROR){
		cerr << "Socket binding Failed : " << WSAGetLastError() << endl;
		freeaddrinfo(addResult);
		closesocket(_listen_socket);
		WSACleanup();
		return false;
	}
	else{
		cout << "Ok." << endl;
	}

	freeaddrinfo(addResult);

	cout << "Start listening...";
	result = listen(_listen_socket, SOMAXCONN);
	if (result == SOCKET_ERROR){
		cerr << "Listening Failed : " << WSAGetLastError() << endl;
		closesocket(_listen_socket);
		WSACleanup();
		return false;
	}
	else{
		cout << "Ok." << endl;
	}

	return true;
}

void Server::mainListenLoop(){

	SOCKET acceptedSocket;

	while (1){
		cout << "Awaiting connection...";
		acceptedSocket = accept(_listen_socket, NULL, NULL);
		if (acceptedSocket == INVALID_SOCKET){
			cerr << "Error accepting connection : " << WSAGetLastError() << endl;
		}
		else{
			cout << "Connected." << endl;
			system("PAUSE");
			closesocket(acceptedSocket);
		}

	}


}