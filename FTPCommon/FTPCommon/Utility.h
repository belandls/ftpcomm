#pragma once
#ifdef COMMON_ENUMS_EXPORTS
#define COMMON_ENUMS_API __declspec(dllexport) 
#else
#define COMMON_ENUMS_API __declspec(dllimport) 
#endif
class Utility
{
public:

	 enum Request{
		LIST_FILES = 1,
		PUT_FILE = 2,
		GET_FILE = 3
	};

	COMMON_ENUMS_API Utility();
	~Utility();

	static COMMON_ENUMS_API void test();
};

