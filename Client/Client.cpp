#include "Client.h"

const char * Client::DEFAULT_PORT = "55555";

Client::Client()
{
	_data_socket = INVALID_SOCKET;
}


Client::~Client()
{
}

bool Client::init(){
	WSAData wsaData;
	int result;

	cout << "Initialising WSA...";
	result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != 0){
		cerr << "WSAStartup failed : " << result << endl;
		return false;
	}
	else{
		cout << "OK" << endl;
	}

	return true;
}

bool Client::connect(char* addr){
	int result;
	addrinfo * addResult = NULL, hints;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	result = getaddrinfo(addr, DEFAULT_PORT, &hints, &addResult);
	if (result != 0){
		cerr << "getaddrinfo failed : " << result << endl;
		WSACleanup();
		return false;
	}

	cout << "Trying to connect to " << addr << "...";
	for (addrinfo* i = addResult; i != NULL; i = addResult->ai_next){
		_data_socket = socket(i->ai_family, i->ai_socktype, i->ai_protocol);
		if (_data_socket == INVALID_SOCKET){
			cerr << "Invalid socket : " << WSAGetLastError << endl;
			WSACleanup();
			return false;
		}
		else{
			result = ::connect(_data_socket, i->ai_addr, (int)i->ai_addrlen);
			if (result == SOCKET_ERROR){
				closesocket(_data_socket);
				_data_socket = INVALID_SOCKET;
				cout << "Connection failed..." << endl;
			}
			else{
				cout << "Connected." << endl;
				system("pause");
				return true;
			}
		}

	}
	return false;
}
