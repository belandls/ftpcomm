#pragma once
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <iostream>

#pragma comment (lib, "Ws2_32.lib")

using namespace std;

class Client
{
private:
	const static char* DEFAULT_PORT;
	SOCKET _data_socket;
public:
	Client();
	~Client();

	bool init();
	bool connect(char*);
};

